package hash

// <Identifier / Constant, Position>
class SymbolTable : BasicHashMap<String, Int>() {
    var currentPosition = 1

    /**
     * Returns position of given token in Symbol Table.
     * If token does not exist, Creates it.
     * @param: token
     * @return: Int
     */
    fun pos(token: String): Int {
        if (get(token) == null) {
            put(token, currentPosition)
            currentPosition += 1
        }
        return get(token)!!
    }
}