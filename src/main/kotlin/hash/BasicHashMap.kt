package hash

import java.util.*

/**
 * Generic Basic HashMap implementation
 */
open class BasicHashMap<Key, Value> {
    private data class Entry<Key, Value> (val key: Key, val value: Value)

    private val loadFactor = 0.75

    private var arraySize = 16
    private var noEntries = 0
    private var entries : Array<LinkedList<Entry<Key, Value>>>
            = Array(arraySize) { LinkedList<Entry<Key, Value>>() }

    /**
     * Returns Value of given Key in HashMap
     * @param: key
     * @return: Value
     */
    fun get(key: Key) : Value? {
        val index = calculateHashCode(key)
        val listAtIndex = entries[index]

        return listAtIndex.find { it.key == key }?.value
    }

    /**
     * Replaces existing Value of given Key with given Value.
     * If Key does not exist, Creates it.
     * @param: key
     * @param: value
     */
    fun put(key: Key, value: Value) {
        noEntries += 1
        if (noEntries > arraySize * loadFactor) {
            increaseCapacity()
        }
        putInto(key, value, entries)
    }

    private fun putInto(key: Key, value: Value, theseEntries: Array<LinkedList<Entry<Key, Value>>>) {
        val index = calculateHashCode(key)
        val listAtIndex = theseEntries[index]

        val entry = Entry(key, value)
        val indexInList = listAtIndex.indexOfFirst { it.key == key }

        if (indexInList >= 0) {
            listAtIndex[indexInList] = entry
        } else {
            listAtIndex.offer(entry)
        }
    }

    private fun calculateHashCode(key: Key): Int {
        return key.hashCode() % arraySize
    }

    private fun increaseCapacity() {
        arraySize *= 2

        val localEntries: Array<LinkedList<Entry<Key, Value>>>
                = Array(arraySize) { LinkedList<Entry<Key, Value>>() }
        noEntries = 0
        entries.forEach { it.forEach{ entry ->
                putInto(entry.key, entry.value, localEntries)
        } }
        entries = localEntries
    }

    override fun toString(): String {
        var str : String = arraySize.toString() + "\n"
        str += noEntries.toString() + "\n"
        entries.forEach {
            it.forEach { entry -> str += "$entry, " }
            str += "\n"
        }
        return str
    }
}
