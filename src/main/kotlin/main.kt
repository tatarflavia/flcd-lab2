import hash.SymbolTable

fun main(args: Array<String>) {
    val map: SymbolTable = SymbolTable()

    map.pos("b")
    map.pos("c")
    map.pos("a")

    println(map)
}